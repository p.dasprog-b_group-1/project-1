//Konversi Jarak dari CM ke KM, M, dan CM
//Header
#include <conio.h>
#include <iostream>

using namespace std;
main(){
    //Variabel
        int km,m,cm;
   
    //Inputan
        cout<<"Konversi Jarak dari cm ke km, m, dan cm"<<endl;
        cout<<"Masukan Jarak dalam CM : ";
        cin>>cm;
   
    //Proses
        //Mencari jumlah km dan sisa cm
            km=cm/100000;
            cm=cm%100000;
        //Mencari jumlah m dan sisa cm
            m=cm/100;
            cm=cm%100;
   
    //Output
        cout<<km<<" km + "<<m<<" m + "<<cm<<" cm "<<endl;
   
    //End
    getch();
}

