//Mempertukarkan Tripel Bilangan Bulat (x,y,z) menjadi (y,z,x)
//Header
#include <conio.h>
#include <iostream>

using namespace std;
main(){
    //Variabel
        int x,y,z,temp;
   
    //Inputan
        cout<<"Mempertukarkan Tripel Bilangan Bulat (x,y,z) menjadi (y,z,x) "<<endl;
        cout<<"Input x : ";cin>>x;
        cout<<"Input y : ";cin>>y;
        cout<<"Input z : ";cin>>z;
       
    //Output
        cout<<"Tripel Bilangan Bulat Asal ("<<x<<","<<y<<","<<z<<")"<<endl;
   
    //Proses
        temp = x;
        x = y;
        y = z;
        z = temp;
   
    //Output
        cout<<"Tripel Bilangan Bulat Menjadi ("<<x<<","<<y<<","<<z<<")"<<endl;
   
    //End
    getch();
}

