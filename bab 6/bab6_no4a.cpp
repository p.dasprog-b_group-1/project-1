//Membaca Nilai Uang Rupiah Kelipatan 25 dan Menentukan Nilai Tukaran Pecahan Uang Rupiah
//Header
#include <conio.h>
#include <iostream>

using namespace std;
main(){
    //Variabel
        int uang,seribu,seratus,lp,dpl,cek;
  
    menu:
    //Inputan
        cout<<"Membaca Nilai Uang Rupiah Kelipatan 25 dan Menentukan Nilai Tukaran Pecahan Uang Rupiah"<<endl;
        cout<<"Input Jumlah Uang : ";
        cin>>uang;
  
    //Pengecekan apakah uang yang dimasukkan adalah kelipatan 25
        cek = uang%25;
  
    //Membuat pilihan apabila variabel cek benar makan program berlanjut ke tahap berikutnya jika salah maka akan kembali ke awal
    if(cek==0){
    //jika hasilnya benar makan menjalankan perintah dibawah ini
        //Proses
            //Menghitung jumlah uang seribu dan sisa uang
                seribu=uang/1000;
                uang=uang%1000;
            //Menghitung jumlah uang seratus dan sisa uang
                seratus=uang/100;
                uang=uang%100;
            //Menghitung jumlah uang lima puluh dan sisa uang
                lp=uang/50;
                uang=uang%50;
            //Menghitung jumlah uang dua puluh lima
                dpl=uang/25;
        //Output
            cout<<seribu<<" buah pecahan Rp 1000, "<<seratus<<" buah pecahan Rp 100, "<<lp<<" buah pecahan Rp 50, dan "<<dpl<<" buah pecahan Rp 25 "<<endl;
    }else{
    //jika hasilnya salah makan menjalankan perintah dibawah ini
        cout<<"Maaf uang yang anda inputkan bukan kelipatan 25"<<endl;
        getch();
        goto menu;
    }
  
    //End
    getch();
}

