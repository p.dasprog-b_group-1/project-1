#include <iostream>
using namespace std;
int main (){
	int A [5];
	cout << "\t======================\t" << endl;
	cout << "\t	MEDIAN	\t" << endl;
	cout << "\t======================\t" << endl;
	
	// input
	for (int i=0;i<5;i++){
		cout << "Masukkan Data ke - " << i+1 << ": ";
		cin >> A [i];
		cout << endl;
	}
	
	//ouput
	cout << "\t === Data Yang Anda Masukkan === \t" << endl;
	for (int i=0;i<5;i++){
		cout << A [i] << " ";
	}
	cout << endl;
	cout << "Median Dari Data adalah " << A[2] << endl;

	return 0;
}
