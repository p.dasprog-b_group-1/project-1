#include <iostream>
#include <bits/stdc++.h>
#include <conio.h>

using namespace std;

main(){
	awal: system("cls");
	string s="";
	int mulai, akhir;
	
	cout<<"Masukkan Kata (Tidak boleh pakai spasi): "; cin>>s;
	
    char arr[s.length() + 1];
    strcpy(arr, s.c_str());
    
    cout<<"\nPotong kata "<<s<<endl
    	<<"Mulai dari huruf ke: "; cin>>mulai;
    cout<<"Sampai huruf ke: "; cin>>akhir;
    
    int len = sizeof(arr)/sizeof(arr[0]);
    
    if(mulai < len && akhir < len){
    	cout<<"\nHasil: ";
    	for(int i = mulai-1; i<akhir; i++) cout<<arr[i];
		cout<<"\n\nTekan enter untuk kembali";
		getch();
		goto awal;
	}
	else{
		cout<<"Kesalahan memotong kata, tekan enter untuk kembali";
		getch();
		goto awal;
	}
}
